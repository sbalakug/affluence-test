import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AvailabilityResponse } from '../model/AvailabilityResponse.model';

@Injectable({
  providedIn: 'root'
})

export class ReservationService {
  url = "http://localhost:8080/resource/"


  constructor(private http: HttpClient) { }

  checkAvailability(ressourceID: number, dateTime: Date): Observable<AvailabilityResponse>{
    
    return this.http.get<AvailabilityResponse>(this.url + ressourceID + "/available?", {params: {datetime: dateTime.toISOString()}});
    
    
  }
}
